So far I am always looking forward to attend the seminar and I am enjoying it overall.
I like how you are showing live demo to almost each topic and try to ask us questions about it.
I finally understand how remotes work, how to use ssh keys and how to work with branches.
However, there are not many examples of situations where things go wrong and we encounter conflicts or some other errors. I would like to see them too and get to know how to fix them.
